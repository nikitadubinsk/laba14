import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { People } from '../app.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { PeopleService } from '../shared/people.service';


@Component({
  selector: 'app-contacts-form',
  templateUrl: './contacts-form.component.html',
  styleUrls: ['./contacts-form.component.css'],
})
export class ContactsFormComponent implements OnInit {

  form : FormGroup;


  @Output() onAdd: EventEmitter<People> = new EventEmitter<People>()

  name = "";
  surname = "";
  telephone = "";
  disabled = true;
  i = 0;
  public myModel = '';
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];


  constructor(private peopleService: PeopleService) {}

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(1)]),
      surname: new FormControl('', [Validators.required, Validators.minLength(1)]),
      telephone: new FormControl('', [Validators.required, Validators.minLength(11)])
    });
  }

  submit() {
    const formData = {...this.form.value}
    this.form.reset()
    const People : People = {
      name: formData.name,
      surname: formData.surname,
      telephone: formData.telephone,
    }
    this.disabled = false;
    this.peopleService.addHuman(People).subscribe(p => {
      this.onAdd.emit(People);
    })
  }

}
