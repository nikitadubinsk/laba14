import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { People } from '../app.component';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private http: HttpClient) { }

  addHuman(people: People): Observable<People[]> {
    return this.http.post<People[]>('http://localhost:3000/contacts', people);
  }

  getAllPeople(): Observable<People[]> {
    return this.http.get<People[]>('http://localhost:3000/contacts');
  }

  deleteHuman(p: People): Observable<void> {
    return this.http.delete<void>(`http://localhost:3000/contacts/${p.id}`);
  }

  editHuman(p): Observable<People[]> {
    return this.http.put<People[]>(`http://localhost:3000/contacts/${p.id}`, {
      name: p.name,
      surname: p.surname,
      telephone: p.telephone
    })
  }
}
