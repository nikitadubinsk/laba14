import { Component } from '@angular/core';
import { CompileShallowModuleMetadata } from '@angular/compiler';
import { HttpClient } from '@angular/common/http';
import { PeopleService } from './shared/people.service';

export interface People {
  name: string
  surname: string
  telephone: string
  id?: number
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'contactsBook';

  search = "";
  loading = false;
  people: People[] = [];
  
  constructor(private peopleService: PeopleService) {}

  ngOnInit() {
    this.loading = true;
    this.peopleService.getAllPeople().subscribe(p => {
      this.people = p;
      this.loading = false;
    });
  }



  updatePeople(p: People) {
    if (this.people.length == 0) {
      p.id = 1;
    } else p.id = this.people[this.people.length-1].id + 1 ;
    this.people.push(p);
  }

  deletePeople(p: People) {
    this.peopleService.deleteHuman(p).subscribe(res => {})
    let index = this.people.findIndex((el)=>el.id==p.id)
    this.people.splice(index, 1);
  }

  savePeople(p: People) {
    let index = this.people.findIndex((el)=>el.id==p.id)
    this.people.splice(index, 1, p);
  }
}
